package willydekeyser.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TBL_USER", schema = "COLLECTOR_CORE")
public class User {

    @Id
    private long userId;

    private String username;

    private String password;

    @Column(name = "status_cd")
    private String statusCd;
}
