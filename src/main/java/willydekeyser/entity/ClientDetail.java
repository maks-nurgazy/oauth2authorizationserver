package willydekeyser.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "OAUTH_CLIENT_DETAILS", schema = "COLLECTOR_CORE")
public class ClientDetail {

    @Id
    @Column(name = "client_id")
    private String clientId;

    @Column(name = "scope")
    private String scope;

    @Column(name = "client_secret")
    private String secret;

    @Column(name = "AUTHORIZED_GRANT_TYPES")
    private String grantTypes;
}
