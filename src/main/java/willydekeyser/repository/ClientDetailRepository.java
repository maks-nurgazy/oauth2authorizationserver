package willydekeyser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import willydekeyser.entity.ClientDetail;

import java.util.Optional;

@Repository
public interface ClientDetailRepository extends JpaRepository<ClientDetail, String> {
    Optional<ClientDetail> findByClientId(String clientId);
}
