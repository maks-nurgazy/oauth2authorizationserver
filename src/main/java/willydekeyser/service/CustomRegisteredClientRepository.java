package willydekeyser.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.stereotype.Service;
import willydekeyser.entity.ClientDetail;
import willydekeyser.repository.ClientDetailRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomRegisteredClientRepository implements RegisteredClientRepository {

    private final ClientDetailRepository clientDetailRepository;
    private final TokenSettings tokenSettings;
    private final ClientSettings clientSettings;

    @Override
    public void save(RegisteredClient registeredClient) {

    }

    @Override
    public RegisteredClient findById(String id) {
        Optional<ClientDetail> optionalClientDetail = clientDetailRepository.findById(id);

        if (optionalClientDetail.isEmpty()) return null;

        ClientDetail clientDetail = optionalClientDetail.get();

        return getRegisteredClient(clientDetail);
    }

    private RegisteredClient getRegisteredClient(ClientDetail clientDetail) {
        return RegisteredClient.withId(clientDetail.getClientId())
                .clientId(clientDetail.getClientId())
                .clientSecret(clientDetail.getSecret())
                .scope(clientDetail.getScope())
                .redirectUri("http://127.0.0.1:8080/login/oauth2/code/users-client-oidc")
                .redirectUri("http://127.0.0.1:8080/authorized")
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                .authorizationGrantType(AuthorizationGrantType.JWT_BEARER)
                .authorizationGrantType(new AuthorizationGrantType("password"))
                .tokenSettings(tokenSettings)
                .clientSettings(clientSettings)
                .build();
    }

    @Override
    public RegisteredClient findByClientId(String clientId) {
        Optional<ClientDetail> optionalClientDetail = clientDetailRepository.findByClientId(clientId);

        if (optionalClientDetail.isEmpty()) return null;

        ClientDetail clientDetail = optionalClientDetail.get();


        return getRegisteredClient(clientDetail);
    }
}
